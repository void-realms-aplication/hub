# Run the following command to build the app:
### dotnet build

# Run the following .NET Core CLI command in the command shell:
### dotnet run

### Browser (while "dontnet run"): http://localhost:5000/weatherforecast
### Browser (while "dontnet run"): http://localhost:5000/swagger/index.html

# httprepl tool:
 # instal:
 ### dotnet tool install -g Microsoft.dotnet-httprepl
 # conect (while "dontnet run"):
 ### httprepl
 ### connect http://localhost:5000 --openapi /swagger/v1/swagger.json