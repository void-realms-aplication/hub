﻿using System.ComponentModel.Composition;
using HUB.Interfaces;

namespace streetLight
{
    [Export(typeof(ISubscriberFactory))]
    [ExportMetadata("Type", "streetLight")]
    public class StreetLightFactory : ISubscriberFactory
    {
        public ISubscriber Create(int id, string ip)
        {
            return new StreetLight(id, ip);
        }
    }
}