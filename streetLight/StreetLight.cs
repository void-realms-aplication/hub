﻿using System.Collections.Generic;
using HUB.Interfaces;

namespace streetLight
{
    public class StreetLight : ISubscriber
    {
        public StreetLight(int id, string ip)
        {
            Id = id;
            Ip = ip;
            State = new Dictionary<string, object>();
            Interest = new List<string>();
            
            InitState();
            InitInterest();
        }

        public Dictionary<string, object> State { get; set; }
        public List<string> Interest { get; set; }
        public int Id { get; set; }
        public string Ip { get; set; }
        
        public void InitState()
        {
            State.Add("enabled", false);
        }

        public void InitInterest()
        {
            Interest.Add("streetLight");
            Interest.Add("powerGrid");
        }
    }
}
