using System;
using System.Collections.Generic;
using System.Linq;

namespace hub.Handlers
{
    public interface ISubscriberHandler
    {
        public List<Subscriber> Subscribers { get; }
        Subscriber GetSubscriberById(int id);
        List<Subscriber> GetSubscribersByType(Type type);
        Subscriber Subscribe(string type, string ip);
        void DeleteSubscribe(Subscriber subscriberToDelete);
        void DeleteAllSubscribe();
    }

    public class SubscriberHandler : ISubscriberHandler
    {
        public List<Subscriber> Subscribers { get; } = new List<Subscriber>();

        public Subscriber GetSubscriberById(int id)
        {
            return Subscribers.First(s => s.Id == id);
        }

        public List<Subscriber> GetSubscribersByType(Type type)
        {
            return Subscribers.Where(s => s.GetType() == type).ToList();
        }

        public Subscriber Subscribe(string type, string ip)
        {
            var newId = 0;
            if (Subscribers.Any()) newId = Subscribers.Max(s => s.Id) + 1;

            switch (type)
            {
                case "trafficLight":
                    Subscribers.Add(new TrafficLight(newId, ip));
                    break;
                case "streetLight":
                    Subscribers.Add(new StreetLight(newId, ip));
                    break;
                case "oilPump":
                    Subscribers.Add(new OilPump(newId, ip));
                    break;
            }

            return Subscribers[newId];
        }

        public void DeleteSubscribe(Subscriber subscriberToDelete)
        {
            Subscribers.Remove(subscriberToDelete);
        }

        public void DeleteAllSubscribe()
        {
            Subscribers.Clear();
        }
    }
}