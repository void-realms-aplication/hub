using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using Microsoft.Extensions.Logging;

namespace hub.Handlers
{
    public interface IJobHandler
    {
        void CreateJob(List<int> destination, Dictionary<string, object> message);
        void ExecuteJob(Job job);
    }

    public class JobHandler : IJobHandler
    {
        private readonly ILogger<JobHandler> _logger;
        private readonly ISubscriberHandler _subscriberHandler;

        public JobHandler(ILogger<JobHandler> logger, ISubscriberHandler subscriberHandler)
        {
            _logger = logger;
            _subscriberHandler = subscriberHandler;
        }

        public void CreateJob(List<int> destination, Dictionary<string, object> message)
        {
            List<Subscriber> subscribers = destination.Select(id => _subscriberHandler.GetSubscriberById(id)).ToList();
            Job job = new Job();
            job.Message = message;
            foreach (var subscriber in subscribers)
            {
                job.IpAddresses.Add(subscriber.Ip.Equals("::1") ? "127.0.0.1" : subscriber.Ip);
                job.Ids.Add(subscriber.Id);
            }
            
            ExecuteJob(job);
        }
        
        public void ExecuteJob(Job job)
        {
            var s = new HttpServer();

            for (var i = 0; i < job.Ids.Count; i++)
            {
                var subscriber = _subscriberHandler.GetSubscriberById(job.Ids[i]);
                foreach (var messageData in job.Message)
                {
                    if (subscriber.State.Keys.Contains(messageData.Key))
                    {
                        subscriber.State[messageData.Key] = messageData.Value;
                    }
                }
                //subscriber.State = job.Message;
                _logger.LogInformation(
                    "message send to '{IpAddress}' for subscriber {Id}: {Message}",
                    job.IpAddresses[i], job.Ids[i], JsonSerializer.Serialize(new {id = job.Ids[i], data = job.Message})
                    );
                s.Send("http://" + job.IpAddresses[i], JsonSerializer.Serialize(new {id = job.Ids[i], data = subscriber.State}));
            }
        }
    }
}