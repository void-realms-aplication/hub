namespace hub
{
    public class StreetLight : Subscriber
    {
        public StreetLight(int id, string ip) : base(id, ip)
        {
        }

        protected override void InitStates()
        {
            State.Add("enabled", true);
        }

        protected override void InitInterest()
        {
            Interest.Add("streetLight");
            Interest.Add("powerGrid");
        }
    }
}