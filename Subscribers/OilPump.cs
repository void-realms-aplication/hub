namespace hub
{
    public class OilPump : Subscriber
    {
        public OilPump(int id, string ip) : base(id, ip)
        {
        }

        protected override void InitStates()
        {
            State.Add("smoke", false);
            State.Add("speed", 0);
        }

        protected override void InitInterest()
        {
            Interest.Add("oilPump");
            Interest.Add("powerGrid");
        }
    }
}