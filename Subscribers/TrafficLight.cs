namespace hub
{
    public class TrafficLight : Subscriber
    {
        public TrafficLight(int id, string ip) : base(id, ip)
        {
        }

        protected override void InitStates()
        {
            State.Add("Green", false);
            State.Add("Orange", false);
            State.Add("Red", false);
        }

        protected override void InitInterest()
        {
            Interest.Add("trafficLight");
            Interest.Add("powerGrid");
        }
    }
}