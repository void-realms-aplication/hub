using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace hub
{
    public abstract class Subscriber
    {
        public Subscriber(int id, string ip)
        {
            State = new Dictionary<string, object>();
            Interest = new List<string>();
            Id = id;
            Ip = ip;

            InitStates();
            InitInterest();
        }

        [JsonPropertyName("id")] public int Id { get; protected set; }

        [JsonPropertyName("ip")] public string Ip { get; protected set; }

        [JsonPropertyName("state")] public Dictionary<string, object> State { get; set; }

        [JsonPropertyName("interest")] public List<string> Interest { get; protected set; }

        protected abstract void InitStates();
        protected abstract void InitInterest();
    }
}