﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;

namespace hub
{
    public class HttpServer
    {
        private readonly HttpClient _httpClient = new HttpClient();
        
        public async void Send(string ip, string msg)
        {
            var httpClient = new HttpClient();

            try
            {
                var response = await httpClient.PostAsync(
                    ip,
                    new StringContent(msg)
                );
                response.EnsureSuccessStatusCode();
            }
            catch (HttpRequestException e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
