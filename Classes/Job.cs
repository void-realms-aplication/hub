﻿using System.Collections.Generic;

namespace hub
{
    public class Job
    {
        public List<int> Ids = new List<int>();
        public List<string> IpAddresses = new List<string>();
        public Dictionary<string, object> Message = new Dictionary<string, object>();
    }
}