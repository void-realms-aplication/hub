using NUnit.Framework;
using hub.TestDataProvider;
using hub.Handlers;
using HUB.Interfaces;

namespace TestSubscriberHandler
{
    public class Tests
    {
        private ISubscriberHandler _subscriberHandler;
        [SetUp]
        public void Setup()
        {
            _subscriberHandler = new SubscriberFactory();
        }

        [Test]
        public void Subscribe()
        {
            ISubscriber testOilPump = _subscriberHandler.Subscribe("oilPump", "TestSubscriber");
            Assert.AreEqual("TestSubscriber", testOilPump.Ip);
        }

        [Test]
        public void GetSubscriberById()
        {
            ISubscriber testOilPump = _subscriberHandler.Subscribe("oilPump", "TestSubscriber");
            Assert.True(_subscriberHandler.GetSubscriberById(testOilPump.Id) == testOilPump);
        }

        [Test]
        public void DeleteSubscribe()
        {
            ISubscriber testOilPump = _subscriberHandler.Subscribe("oilPump", "TestSubscriber");
            _subscriberHandler.DeleteSubscribe(testOilPump);
            Assert.False(_subscriberHandler.Subscribers.Contains(testOilPump));
        }

        [Test]
        public void DeleteAllSubscribe()
        {
            _subscriberHandler.DeleteAllSubscribe();
            Assert.True(_subscriberHandler.Subscribers.Count == 0);
        }
    }
}