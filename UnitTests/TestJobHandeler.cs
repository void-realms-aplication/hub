using System.Collections.Generic;
using NUnit.Framework;
using hub.TestDataProvider;
using hub.Handlers;
using HUB.Interfaces;
using Microsoft.Extensions.Logging.Abstractions;

namespace TestJobHandeler
{
    public class Tests
    {
        private ISubscriberHandler _subscriberHandler;
        private IJobHandler _jobHandler;
        [SetUp]
        public void Setup()
        {
            
            _subscriberHandler = new SubscriberFactory();
            _jobHandler = new JobHandler(NullLogger<JobHandler>.Instance, _subscriberHandler);
        }

        [Test]
        public void CreateJob()
        {
            List<int> ids = new List<int> {0};
            Dictionary<string, object> message = new Dictionary<string, object> {{"smoke", true}, {"speed", 150}};
            _jobHandler.CreateJob(ids, message);
            ISubscriber testSubscriber = _subscriberHandler.GetSubscriberById(0);
            Assert.True(testSubscriber.State["smoke"].Equals(true) && testSubscriber.State["speed"].Equals(150));
        }
    }
}