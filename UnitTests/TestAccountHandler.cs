using System.Collections.Generic;
using NUnit.Framework;
using hub.TestDataProvider;
using hub.Handlers;
using Microsoft.Extensions.Logging.Abstractions;

namespace TestAccountHandler
{
    public class Test
    {
        private IAccountHandler _accountHandler;
        [SetUp]
        public void Setup()
        {
            _accountHandler = new AccountFactory(NullLogger<AccountHandler>.Instance);
        }
        
        private KeyValuePair<string, string> testUser;
        
        [Test]
        public void VerifyAccount1()
        {
            testUser = new KeyValuePair<string, string>("password", "password");
            string token = _accountHandler.VerifyAccount(testUser);
            Assert.True(token.Equals("wrong username"));
        }
        
        [Test]
        public void VerifyAccount2()
        {
            testUser = new KeyValuePair<string, string>("admin", "password");
            string token = _accountHandler.VerifyAccount(testUser);
            Assert.True(token.Equals("wrong password"));
        }
        
        [Test]
        public void VerifyAccount3()
        {
            testUser = new KeyValuePair<string, string>("admin", "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918");
            string token = _accountHandler.VerifyAccount(testUser);
            Assert.True(!(token.Equals("wrong username") || token.Equals("wrong password")));
        }
    }
}