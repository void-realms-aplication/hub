using hub.Controllers;
using hub.TestDataProvider;
using NUnit.Framework;
using hub.Handlers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging.Abstractions;

namespace TestAccountController
{
    public class TestA
    {
        private IAccountHandler _accountHandler;
        private ControllerBase _accountController;
        
        [SetUp]
        public void Setup()
        {
            _accountHandler = new AccountFactory(NullLogger<AccountHandler>.Instance);
            _accountController = new AccountController(NullLogger<AccountController>.Instance, _accountHandler);
        }
    }
}