﻿using System.Collections.Generic;

namespace HUB.Interfaces
{
    public interface ISubscriber
    {
        Dictionary<string, object> State { get; set; }
        List<string> Interest { get; set; }
        int Id { get; set; }
        string Ip { get; set; }

        void InitState();
        void InitInterest();
    }
}
