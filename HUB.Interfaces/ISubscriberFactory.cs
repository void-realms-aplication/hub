﻿namespace HUB.Interfaces
{
    public interface ISubscriberFactory
    {
        ISubscriber Create(int id, string ip);
    }

    public interface IFactoryData
    {
        string Type { get; }
    }
}