﻿using System.ComponentModel.Composition;
using HUB.Interfaces;

namespace trafficLight
{
    [Export(typeof(ISubscriberFactory))]
    [ExportMetadata("Type", "trafficLight")]
    public class TrafficLightFactory : ISubscriberFactory
    {
        public ISubscriber Create(int id, string ip)
        {
            return new TrafficLight(id, ip);
        }
    }
}