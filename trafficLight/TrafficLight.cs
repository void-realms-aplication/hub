using System.Collections.Generic;
using System.ComponentModel.Composition;
using HUB.Interfaces;

namespace trafficLight
{
    public class TrafficLight : ISubscriber
    {
        public TrafficLight(int id, string ip)
        {
            Id = id;
            Ip = ip;
            State = new Dictionary<string, object>();
            Interest = new List<string>();

            InitState();
            InitInterest();
        }

        public Dictionary<string, object> State { get; set; }
        public List<string> Interest { get; set; }
        public int Id { get; set; }
        public string Ip { get; set; }

        public void InitState()
        {
            State.Add("Green", false);
            State.Add("Orange", false);
            State.Add("Red", false);
        }

        public void InitInterest()
        {
            Interest.Add("trafficLight");
            Interest.Add("powerGrid");
        }
    }
}