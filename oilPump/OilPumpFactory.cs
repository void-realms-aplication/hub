﻿using System.ComponentModel.Composition;
using HUB.Interfaces;

namespace oilPump
{
    [Export(typeof(ISubscriberFactory))]
    [ExportMetadata("Type", "oilPump")]
    public class OilPumpFactory : ISubscriberFactory
    {
        public ISubscriber Create(int id, string ip)
        {
            return new OilPump(id, ip);
        }
    }
}