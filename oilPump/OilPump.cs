using System.Collections.Generic;
using HUB.Interfaces;

namespace oilPump
{
    public class OilPump : ISubscriber
    {
        public OilPump(int id, string ip)
        {
            Id = id;
            Ip = ip;
            State = new Dictionary<string, object>();
            Interest = new List<string>();

            InitState();
            InitInterest();
        }
        
        public Dictionary<string, object> State { get; set; }
        public List<string> Interest { get; set; }
        public int Id { get; set; }
        public string Ip { get; set; }

        public void InitState()
        {
            State.Add("smoke", false);
            State.Add("speed", 0);
        }

        public void InitInterest()
        {
            Interest.Add("oilPump");
            Interest.Add("powerGrid");
        }
    }
}