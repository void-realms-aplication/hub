using hub.Controllers;
using hub.Handlers;
using Microsoft.Extensions.Logging;

namespace hub.TestDataProvider
{
    public class AccountFactory : AccountHandler
    {
        public AccountFactory(ILogger<AccountHandler> logger) : base(logger)
        {
            Accounts.Add("admin", "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918");
        }
    }
}