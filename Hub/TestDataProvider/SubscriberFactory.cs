﻿using hub.Handlers;

namespace hub.TestDataProvider
{
    public class SubscriberFactory : SubscriberHandler
    {
        // public SubscriberFactory(ILogger<SubscriberHandler> logger): base(logger)
        public SubscriberFactory()
        {
            CreateContainer();
            foreach (var i in SubscriberFactories)
            {
                Subscribe(i.Metadata.Type, "factoryCreated");
                Subscribe(i.Metadata.Type, "factoryCreated");
            }
        }
    }
}