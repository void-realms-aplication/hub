using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using HUB.Interfaces;

namespace hub.Handlers
{
    public interface ISubscriberHandler
    {
        public List<ISubscriber> Subscribers { get; }
        ISubscriber GetSubscriberById(int id);
        List<ISubscriber> GetSubscribersByType(Type type);
        ISubscriber Subscribe(string type, string ip);
        void DeleteSubscribe(ISubscriber subscriberToDelete);
        void DeleteAllSubscribe();
    }
    
    public class SubscriberHandler : ISubscriberHandler
    {
        [ImportMany(typeof(ISubscriberFactory))] 
        public IEnumerable<Lazy<ISubscriberFactory, IFactoryData>> SubscriberFactories;

        public List<ISubscriber> Subscribers { get; } = new List<ISubscriber>();
        
        private CompositionContainer _container;

        protected void CreateContainer()
        {
            try
            {
                // An aggregate catalog that combines multiple catalogs.
                var catalog = new AggregateCatalog();
                // Adds all the parts found in the same assembly as the Program class.
                catalog.Catalogs.Add(new DirectoryCatalog("Extensions"));

                // Create the CompositionContainer with the parts in the catalog.
                _container = new CompositionContainer(catalog);
                _container.ComposeParts(this);
            }
            catch (CompositionException compositionException)
            {
                Console.WriteLine(compositionException.ToString());
            }
        }

        public ISubscriber GetSubscriberById(int id)
        {
            return Subscribers.First(s => s.Id == id);
        }

        public List<ISubscriber> GetSubscribersByType(Type type)
        {
            return Subscribers.Where(s => s.GetType() == type).ToList();
        }
        
        public ISubscriber Subscribe(string type, string ip)
        {
            CreateContainer();
            var newId = 0;
            if (Subscribers.Any())
            {
                newId = Subscribers.Max(s => s.Id) + 1;
            }

            foreach (var i in SubscriberFactories)
            {
                if (i.Metadata.Type == type)
                {
                    Subscribers.Add(i.Value.Create(newId, ip));
                }
            }

            return Subscribers[newId]; 
        }

        public void DeleteSubscribe(ISubscriber subscriberToDelete)
        {
            Subscribers.Remove(subscriberToDelete);
        }

        public void DeleteAllSubscribe()
        {
            Subscribers.Clear();
        }
    }
}