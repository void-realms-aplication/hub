using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;

namespace hub.Handlers
{
    public interface IAccountHandler
    {
        public Dictionary<string, string> Accounts { get; }
        public Dictionary<string, string> Tokens { get; }
        void AddAccounts(Dictionary<string, string> accountsToAdd);
        string VerifyAccount(KeyValuePair<string, string> accountsToVerify);
    }

    public class AccountHandler : IAccountHandler
    {
        private readonly ILogger<AccountHandler> _logger;
        public AccountHandler(ILogger<AccountHandler> logger)
        {
            _logger = logger;
        }

        public Dictionary<string, string> Accounts { get; } = new Dictionary<string, string>();
        public Dictionary<string, string> Tokens { get; } = new Dictionary<string, string>();

        public void AddAccounts(Dictionary<string, string> accountsToAdd)
        {
            foreach (var (name, password) in accountsToAdd)
            {
                Accounts.Add(name, password);
            }
        }

        public string VerifyAccount(KeyValuePair<string, string> accountsToVerify)
        {
            if (!Accounts.Keys.Contains(accountsToVerify.Key)) return "wrong username";
            if (!Accounts[accountsToVerify.Key].Equals(accountsToVerify.Value)) return "wrong password";
            if (Tokens.ContainsKey(accountsToVerify.Key))
                return Tokens[accountsToVerify.Key];
            var token = Guid.NewGuid().ToString();
            Tokens.Add(accountsToVerify.Key, token);
            return token;
        }
    }
}