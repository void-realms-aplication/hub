#nullable enable
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Extensions.Logging;

namespace hub.Handlers
{
    public interface IScenarioHandler
    {
        Dictionary<string, object?> GetScenarios();
        object? GetScenarioByName(string name);
    }

    public class ScenarioHandler : IScenarioHandler
    {
        public Dictionary<string, object?> Scenarios = new Dictionary<string, object?>();
            
        private readonly ILogger<ScenarioHandler> _logger;

        public ScenarioHandler(ILogger<ScenarioHandler> logger)
        {
            _logger = logger;
            LoadScenariosFromScenarioClass();
        }

        public Dictionary<string, object?> GetScenarios()
        {
            return Scenarios;
        }
        
        public object? GetScenarioByName(string name)
        {
            return Scenarios[name];
        }

        private void LoadScenariosFromScenarioClass()
        {
            var types = Assembly
                .GetExecutingAssembly()
                .GetTypes()
                .Where(t => t.Namespace != null && t.Namespace.StartsWith("hub.Scenarios"))
                .ToArray();
                _logger.LogInformation("types: " + types);
            
            foreach (var t in types)
            {
                Scenarios.Add(t.Name, Activator.CreateInstance(t));
                _logger.LogInformation("added scenario with name " + t.Name);
            }
        }
    }
}