using System;
using System.Collections.Generic;
using System.Text.Json;
using hub.Handlers;
using HUB.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace hub.Controllers
{
    [EnableCors]
    [ApiController]
    [Route("[controller]")]
    public class SubscriberController : ControllerBase
    {
        private readonly ILogger<SubscriberController> _logger;
        private readonly ISubscriberHandler _subscriberHandler;

        public SubscriberController(ILogger<SubscriberController> logger, ISubscriberHandler subscriberHandler)
        {
            _logger = logger;
            _subscriberHandler = subscriberHandler;
        }

        [HttpGet]
        public IEnumerable<ISubscriber> Get()
        {
            return _subscriberHandler.Subscribers;
        }

        [HttpGet("{id:int}")]
        public ISubscriber Get(int id)
        {
            return _subscriberHandler.GetSubscriberById(id);
        }

        [HttpPost]
        public Dictionary<string, object> AddSubscriber([FromBody] string type)
        {

            var remoteIp = Request.HttpContext.Connection.RemoteIpAddress;

            if (remoteIp == null) throw new Exception("IP not found");
            
            var newSubscriber = _subscriberHandler.Subscribe(type, remoteIp.ToString());
            _logger.LogInformation(
                "added subscriber: {NewSubscriber}",
                JsonSerializer.Serialize(newSubscriber)
            );

            return new Dictionary<string, object>
            {
                {"id", newSubscriber.Id}, 
                {"data", newSubscriber.State}
            };
        }
        
        [HttpDelete("{id:int}")]
        public ActionResult DeleteSubscriber(int id)
        {
            try
            {
                var subscriberToDelete = _subscriberHandler.GetSubscriberById(id);

                if (subscriberToDelete == null)
                {
                    return NotFound($"Subscriber with Id = {id} not found");
                }
                _subscriberHandler.DeleteSubscribe(subscriberToDelete);
                return NoContent();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }
        
        [HttpDelete]
        [Route("ids")]
        public ActionResult DeleteSubscriber(int[] ids)
        {
            try
            {
                foreach (var id in ids)
                {
                    var subscriberToDelete = _subscriberHandler.GetSubscriberById(id);

                    if (subscriberToDelete == null)
                    {
                        return NotFound($"Subscriber with Id = {id} not found");
                    }
                    _subscriberHandler.DeleteSubscribe(subscriberToDelete);
                }
                return NoContent();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }
        
        [HttpDelete]
        public ActionResult DeleteAllSubscriber()
        {
            try
            {
                var subscribersToDelete = _subscriberHandler.Subscribers;

                if (subscribersToDelete == null)
                {
                    return NotFound($"No Subscribers found");
                }
                _subscriberHandler.DeleteAllSubscribe();
                return NoContent();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }
    }
}