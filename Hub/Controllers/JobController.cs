﻿using System.ComponentModel.Composition;
using System.Text.Json;
using HUB;
using hub.Handlers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace hub.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class JobController : ControllerBase
    {
        private readonly IJobHandler _jobHandler;
        
        private readonly ILogger<JobController> _logger;

        public JobController(ILogger<JobController> logger, IJobHandler jobHandler)
        {
            _logger = logger;
            _jobHandler = jobHandler;
        }

        [HttpPost]
        public NoContentResult AddJob([FromBody] JobRequestBody jobRequestBody)
        {
            var s = new HttpServer();
            _logger.LogInformation(
                "added job: {SerializedJobRequestBody}",
                JsonSerializer.Serialize(jobRequestBody)
            );
            _jobHandler.CreateJob(jobRequestBody.Destination, jobRequestBody.Message);

            return NoContent();
        }
    }
}