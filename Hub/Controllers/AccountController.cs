using System.Collections.Generic;
using System.Text.Json;
using hub.Handlers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using JsonSerializer = System.Text.Json.JsonSerializer;

namespace hub.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AccountController : ControllerBase
    {
        private readonly ILogger<AccountController> _logger;
        private readonly IAccountHandler _accountHandler;

        public AccountController(ILogger<AccountController> logger, IAccountHandler accountHandler)
        {
            _logger = logger;
            _accountHandler = accountHandler;
        }

        [HttpGet]
        public Dictionary<string, string> Get()
        {
            _logger.LogInformation(JsonSerializer.Serialize(_accountHandler.Accounts));
            return _accountHandler.Accounts;
        }
        
        [HttpPost]
        public NoContentResult AddAccounts([FromBody] AddAccountsRequestBody RequestBody)
        {
            return NoContent();
        }
        public class AddAccountsRequestBody
        {
            public KeyValuePair<string, string> TokenAccount;
            public Dictionary<string, string> AccountsToAdd;
        }
        
        [HttpPost]
        [Route("Verify")]
        public string VerifyAccount([FromBody] JsonDocument RequestBody)
        {
            var verifyRequestBody = JsonConvert.DeserializeObject<VerifyAccountRequestBody>(JsonSerializer.Serialize(RequestBody));
            return _accountHandler.VerifyAccount(verifyRequestBody.UserAccount);
        }
        public class VerifyAccountRequestBody
        {
            public KeyValuePair<string, string> UserAccount;
            
        }
        
        
        [HttpPost]
        [Route("VerifyToken")]
        public bool VerifyToken([FromBody] JsonDocument RequestBody)
        {
            var verifyRequestBody = JsonConvert.DeserializeObject<VerifyAccountRequestBody>(JsonSerializer.Serialize(RequestBody));
            return _accountHandler.Tokens[verifyRequestBody.UserAccount.Key].Equals(verifyRequestBody.UserAccount.Value);
        }
    }
}