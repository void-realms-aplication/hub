using System;
using System.Collections.Generic;
using System.Text.Json;
using hub.Handlers;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace hub.Controllers
{
    [EnableCors]
    [ApiController]
    [Route("[controller]")]
    public class ScenarioController : ControllerBase
    {
        private readonly ILogger<ScenarioController> _logger;
        private readonly IScenarioHandler _scenarioHandler;

        public ScenarioController(ILogger<ScenarioController> logger, IScenarioHandler scenarioHandler)
        {
            _logger = logger;
            _scenarioHandler = scenarioHandler;
        }

        [HttpGet]
        public string Get()
        {
            return JsonConvert.SerializeObject(_scenarioHandler.GetScenarios());
        }

        [HttpGet("{name}")]
        public object Get(string name)
        {
            return _scenarioHandler.GetScenarioByName(name);
        }
    }
}