﻿using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace HUB
{
    public class JobRequestBody
    {
        [JsonPropertyName("destination")] public List<int> Destination { get; set; }

        [JsonPropertyName("message")] public Dictionary<string, object> Message { get; set; }
    }
}