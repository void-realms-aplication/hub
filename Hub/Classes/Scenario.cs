using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace hub
{
    public abstract class Scenario
    {
        public Scenario(int id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
        }

        [JsonPropertyName("id")] public int Id { get; protected set; }

        [JsonPropertyName("name")] public string Name { get; protected set; }
        
        [JsonPropertyName("description")] public string Description { get; protected set; }

        protected abstract void start();
    }
}